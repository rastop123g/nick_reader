# Импорты библиотек
import keyboard
import mouse
import numpy as np
import pyautogui
import imutils
import cv2
import pytesseract

def process():
  position = mouse.get_position()
  print(position)
  image = pyautogui.screenshot(region=(position[0] - 150, position[1] - 20, 300, 40))
  print(pytesseract.image_to_string(image))

# Улавнивание комбинации
keyboard.add_hotkey('shift+z', process)
keyboard.wait()